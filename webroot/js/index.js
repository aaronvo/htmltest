$(document).ready(function() {

    var owl = $("#owl-slider");

    owl.owlCarousel({

        items : 4, //10 items above 1000px browser width
        itemsDesktop : [1000,4], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
        itemsTablet: [600,2], //2 items between 600 and 0;
        itemsMobile : true // itemsMobile disabled - inherit from itemsTablet option

    });

    // Custom Navigation Events
    $(".next").click(function(){
        owl.trigger('owl.next');
    });
    $(".prev").click(function(){
        owl.trigger('owl.prev');
    });
    $(".play").click(function(){
        owl.trigger('owl.play',1000);
    });
    $(".stop").click(function(){
        owl.trigger('owl.stop');
    });


    var owl2 = $("#owl-slider_2");

    owl2.owlCarousel({

        items : 4, //10 items above 1000px browser width
        itemsDesktop : [1000,4], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
        itemsTablet: [600,2], //2 items between 600 and 0;
        itemsMobile : true // itemsMobile disabled - inherit from itemsTablet option

    });

    // Custom Navigation Events
    $(".next").click(function(){
        owl2.trigger('owl.next');
    });
    $(".prev").click(function(){
        owl2.trigger('owl.prev');
    });
    $(".play").click(function(){
        owl2.trigger('owl.play',1000);
    });
    $(".stop").click(function(){
        owl2.trigger('owl.stop');
    });


    $("li.section1").click(function() {
        $('html, body').animate({
            scrollTop: $(".section_1").offset().top
        }, 500);
    });

    $("li.section2").click(function() {
        $('html, body').animate({
            scrollTop: $(".section_2").offset().top
        }, 500);
    });

    $("li.section3").click(function() {
        $('html, body').animate({
            scrollTop: $(".section_3").offset().top
        }, 500);
    });
    $("button.lorem").click(function() {
        $('html, body').animate({
            scrollTop: $(".after_content").offset().top
        }, 500);
    });
});