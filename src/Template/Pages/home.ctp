<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;

if (!Configure::read('debug')):
    throw new NotFoundException();
endif;

$cakeDescription = 'Duc Qui Vo';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>
    <meta name="description" content="testing html skill">
    <meta name="keyword" content="testing , html5, css3, scss">
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('owl.carousel.css') ?>
    <?= $this->Html->css('owl.theme.css') ?>
    <?= $this->Html->css('ducquivo.css') ?>
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('bootstrap-theme.min.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->script('jquery-2.1.4.min.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('owl.carousel.js') ?>
    <?= $this->Html->script('index.js') ?>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <?= $this->Html->css('ie.css') ?>
    <![endif]-->

</head>
<body>
<header>
    <h1><i class="icons-play"></i>Lorem ipsum dolor sit ametconsectetur adipiscing elit.</h1>
</header>
<main>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lacinia viverra efficitur. Nunc lacus risus,
        fringilla maximus ligula at, sagittis accumsan diam. Quisque et suscipit leo. Phasellus sit amet mi sit amet
        magna aliquam porta.
    </p>

    <p>
        Sed non malesuada sapien. Fusce a consectetur ex. Maecenas laoreet malesuada ligula, id auctor metus consectetur et. Morbi euismod in sapien nona accumsa. Nam non ipsum eros. Proin tempus eu felis at laoreet. Maecenas ultrices, nibh id cursus.
    </p>
    <nav>
        <ul>
            <li class="section1"><a href="#chapter-one">eros: laoreet</a></li>
            <li class="section2"><a href="#chapter-two">Praesent: eleifend sodales</a></li>
            <li class="section3"><a href="#chapter-three">Maecenas: laoreet eros</a></li>
        </ul>
    </nav>
</main>
<section id="content">

    <article class="section_1">
        <h2><i class="icons-play"></i>EROS: LAOREET ARCU.</h2>


        <p>
            Donec semper pharetra vulputate. Morbi interdum, mi eget consequat rhoncus, libero mi porta arcu, non
            aliquam ligula orci in arcu.Donec semper pharetra vulputate. Morbi interdum, mi eget consequat rhoncus,
            libero mi porta arcu, non aliquam ligula orci.
        </p>

        <p>
            ETIAM ALIQUAM TRISTIQUE PURUS SIT AMET HENDRERIT. Vestibulum ante
        </p>


        <div id="slider">

            <div class="row">

                <div class="left_button"><a class="btn prev"><!--<i class="icons-angle-left"></i>--></a></div>

                <div id="owl-slider" class="owl-carousel">

                    <div class="item">
                        <i class="icons-pdf"></i>

                        <p>Mauris semper eros felis</p>

                    </div>
                    <div class="item">
                        <i class="icons-pdf"></i>

                        <p>Mauris semper eros felis</p>

                    </div>
                    <div class="item">
                        <i class="icons-pdf"></i>

                        <p>Mauris semper eros felis Quisque et suscipit</p>

                    </div>
                    <div class="item">
                        <i class="icons-pdf"></i>

                        <p>Mauris semper eros felis</p>

                    </div>
                    <div class="item">
                        <i class="icons-pdf"></i>

                        <p>Mauris semper eros felis</p>

                    </div>
                    <div class="item">
                        <i class="icons-pdf"></i>

                        <p>Mauris semper eros felis</p>

                    </div>
                    <div class="item">
                        <i class="icons-pdf"></i>

                        <p>Mauris semper eros felis</p>

                    </div>

                </div>

                <div class="right_button"><a class="btn next"><!--<i class="icons-angle-right"></i>--></a></div>
            </div>

        </div>

        <section class="after_slider">
        <h3>Suspendisse viverra elit at velit luctus:</h3>

        <div class="row">
            <div class="col-sm-4">
                <h4>Egestas eros Vivamus eu:</h4>
                <ul>
                    <li>Nibh rhoncus lectus</li>
                    <li>Praesent et nibh ut non bibendum velit</li>

                </ul>
            </div>
            <div class="col-sm-4">
                <h4>Egestas eros Vivamus eu:</h4>
                <ul>
                    <li>Nibh rhoncus lectus</li>
                    <li>Duis at tellus at ligula eros tellus at ligula</li>
                    <li>Praesent et nibh</li>
                </ul>

            </div>
            <div class="col-sm-4">
                <h4>Egestas eros Vivamus eu:</h4>
                <ul>
                    <li>Eros nibh lectus at</li>
                    <li>Etiam fringilla arcu nec</li>
                    <li>vestibulum accumsan</li>
                    <li> In sagittis lobortis risus, tristique libero</li>
                </ul>
            </div>
        </div>
        </section>
    </article>
    <div class="line"></div>
    <article class="section_2">

        <h2><i class="icons-play"></i>Nunc: ultricies tortor id augue.</h2>

        <p>
            Sed egestas eros id commodo laoreet. Ut sollicitudin erat a tellus feugiat molestie. Vestibulum semper
            vestibulum nulla sed pellentesque. Vivamus eu venenatis ipsum. Sed ac urna quam. Etiam fringilla arcu nec
            nulla molestie dictum. Vivamus in pulvinar elit, quis facilisis nisi. Fusce vel auctor turpis, ut luctus
            quam.
        </p>

        <p>
           Mauris semper eros felis, non bibendum velit gravida quis. Nunc ultricies tortor id augue volutpat, sed
            ornare nisl blandit. Etiam pulvinar ipsum erat, id fringilla purus efficitur id.Mauris semper eros felis,
            non bibendum efficitur id.
        </p>


        <div id="slider_2">

            <div class="row">

                <div class="left_button"><a class="btn prev"><!--<i class="icons-angle-left"></i>--></a></div>

                <div id="owl-slider_2" class="owl-carousel">

                    <div class="item">
                        <i class="icons-pdf"></i>

                        <p>MAuris semper Eros Felis non</p>

                    </div>
                    <div class="item">
                        <i class="icons-pdf"></i>

                       <p>MAuris semper Eros Felis non nunc ultricies tortor id augue nis volutpat sed
                       </p>

                    </div>
                    <div class="item">
                        <i class="icons-pdf"></i>

                      <p>MAuris semper Eros Felis non nunc ultricies tortor id augue nis volutpat sed efficitur ni
                          bibendum eros felis mauris</p>

                    </div>
                    <div class="item">
                        <i class="icons-pdf"></i>

                        <p>MAuris semper Eros Felis non
                            MAuris semper Eros Felis non</p>

                    </div>
                    <div class="item">
                        <i class="icons-pdf"></i>

                        <p>Mauris semper eros felis</p>

                    </div>
                    <div class="item">
                        <i class="icons-pdf"></i>

                        <p>Mauris semper eros felis</p>

                    </div>
                    <div class="item">
                        <i class="icons-pdf"></i>

                        <p>Mauris semper eros felis</p>

                    </div>

                </div>

                <div class="right_button"><a class="btn next"><!--<i class="icons-angle-right"></i>--></a></div>
            </div>

        </div>


    </article>
    <div class="line"></div>
    <article class="section_3">

        <h2><i class="icons-play"></i>Maecenas: laoreet malesuada id.</h2>

        <p>
            Suspendisse viverra elit at velit luctus, interdum varius nibh tempor. Nullam dictum varius lorem quis
            laoreet. Sed cursus tempus iaculis. Nullam tempus metus bibendum odio pretium auctor. Suspendisse viverra
            elit at velit luctus, interdum varius nibh tempor. Nullam dictum varius lorem quis laoreet.

        </p>


        <div class="row">
            <div class="col-md-6 left">
                    <div class="col-xs-6">
                        <img src="img/ppt.jpg" alt="testing image"/>
                    </div>
                        <div class="col-xs-6">
                            <h3>
                                Laoreet Malesuada
                            </h3>

                            <div class="small_text">
                                Varius nibh tempor pretium | id | nibh
                                nullam dictum
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <button class="lorem">
                                Lorem IPSUM
                            </button>
                        </div>
            </div>

            <div class="col-md-6 right">
                <aside>
                    <p>
                        Ut sollicitudin erat a tellus feugiat molestie vestibulum semper. In sagittis lobortis risus, at
                        tristique libero maximus sed. In non urna urna. Etiam aliquam tristique purus sit amet
                        hendrerit.
                        Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.
                    </p>
                    <ul>
                            <li> Eros felis, non bibendum</li>
                            <li> Nunc ultricies tortor id augue</li>
                            <li> Sed egestas eros id commodo</li>
                        </ul>

                    <p>
                        <b>Nullam dictum varius:</b> Nunc id elit magna. Duis congue semper leo, et finibus mauris
                    </p>
                </aside>
            </div>

        </div>
    </article>

</section>
<section class="after_content">
    <h3>Lorem ipsum dolor sit?</h3>

<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
</section>
<footer>
    <div class="icons">
        <i class="icons-facebook"></i>
        <i class="icons-twitter"></i>
        <i class="icons-youtube"></i>
    </div>

    <p>
        Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum
        qui dolorem eum fugiat quo voluptasQuis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam
        nihil molestiae consequatur, vel illum qui dolorem eum fugiat.
    </p>
</footer>
</body>
</html>
